(function ()
{
    "use strict";

    var file = location.pathname.match(/([^/.]+)\.html$/)[1];
    var styles = "data/" + file + ".css";

    if (document.createStyleSheet)
    {
        document.createStyleSheet(styles);
    }
    else if (document.head)
    {
        document.head.insertAdjacentHTML("beforeend", "<link rel='stylesheet' href='" + styles + "'>");
    }
    else
    {
        // If this gets here, the browser is old enough nobody will care how it's done.
        // Especially since it'll only prevent the subsequent error dialog from looking bad.
        document.write("<link rel='stylesheet' href='" + styles + "'>");
    }

    if (!window.Audio)
    {
        // The body tag may not be available at this point, so the error can't be shown until it is.
        (window.addEventListener || window.attachEvent)(window.addEventListener ? "load" : "onload", function ()
        {
            error("HTML5 Audio Error", "This application requires support for HTML5 audio. "
                    + "Try upgrading to the latest version of your browser or trying different one.");
        }, false);
    }

    window.addEventListener && window.addEventListener("load", function ()
    {
        req(function (xml)
        {
            document.title = xml.find("compilation > title").text();

            try
            {
                player(xml.find("part track").sort(function (a, b)
                {
                    var ai = a.getAttribute("index"), bi = b.getAttribute("index");
                    return (ai - bi) || ai.localeCompare(bi);
                }), "compilations/" + xml.find("compilation > path").text());
            }
            catch (e)
            {
                if (console && console.log)
                {
                    console.log(e);
                }
                error("Initialization Error", e.message || "Unknown cause");
            }
        }, file + ".xml");

        if (document.querySelector("#player-info"))
        {
            req(function (info)
            {
                var overlay, resize, click;

                if (!info)
                {
                    info = document.getElementById("player-info");
                    if (info)
                    {
                        info.style.visibility = "hidden";
                    }
                    return;
                }

                overlay = document.createElement("div");
                overlay.id = "player-info-overlay";
                overlay.style.opacity = 0;
                overlay.style.zIndex = -1;
                overlay.innerHTML = "<div>" + sanitize(info).replace(/\r\n|\n/g, "<br>").replace(/<br>$/, "")
                        + "</div>";
                document.body.appendChild(overlay);

                resize = function ()
                {
                    var style, diff;
                    style = parseFloat(window.getComputedStyle(overlay.firstChild).left);
                    diff = overlay.clientHeight - overlay.firstChild.clientHeight;

                    if (diff / 2 >= style)
                    {
                        overlay.firstChild.style.top = diff / 2 + "px";
                        overlay.firstChild.style.bottom = "";
                    }
                    else
                    {
                        overlay.firstChild.style.top = style + "px";
                        overlay.firstChild.style.bottom = style + "px";
                    }
                };
                window.addEventListener("resize", resize, false);
                resize();

                click = function ()
                {
                    var visible = Number(overlay.style.opacity);
                    overlay.style.opacity = (visible ? 0 : 1);

                    if (visible)
                    {
                        // Yes, this should be using a transitionend listener. IE didn't implement that until 10,
                        // Opera changed their prefixed name twice, WebKit spells it differently from everyone else,
                        // and IE10 with Firefox 4+ support the final unprefixed version. It's also a pain opacity
                        // is treated as click-blocking even when the value is zero. Complain complain complain.
                        setTimeout(function ()
                        {
                            overlay.style.zIndex = -1;
                        }, 1000);
                    }
                    else
                    {
                        overlay.style.zIndex = "";
                    }
                };
                button(click, "player-info");
                overlay.addEventListener("click", click, false);
            }, "about.txt", true);
        }
    }, false);

    function player (tracks, path)
    {
        var r, player, playlist, visualizer, scroller, tracklist, progress, box, thumb, time, updating = {};

        playlist = new PlayList(tracks);
        visualizer = document.getElementById("player-visualizer");
        scroller = document.getElementById("player-scroller");

        if (visualizer)
        {
            try
            {
                visualizer = new (window.THREE ? BarGrid3DViz : BarViz)(visualizer);
            } catch (e)
            {
                // Creating a WebGL context failed despite detected support so we fall back. (Chrome seems to do this.)
                if (window.THREE)
                {
                    visualizer = new BarViz(visualizer);
                }
            }
        }

        if (scroller)
        {
            scroller = BasicAnimatedScroller.isSupported() ? new BasicAnimatedScroller(scroller) : new Scroller(scroller);
        }

        req(function (text)
        {
            scroller.defaultText = text || "";
            scroller.update(text);
        }, "default.txt", true);

        tracklist = document.getElementById("player-tracks");
        r = (Math.ceil(tracks.length / 100) * 100 + "").length;

        tracks.forEach(function (track, i)
        {
            track.index = i; // For playlists where the order indexes don't match the UI order.

            var element = document.createElement("li");
            element.setClass("selected", !i);
            element.setAttribute("data-index", i);
            element.textContent = zeros(r, i + 1) + ". " + track.find("name").text(", ") + " - " + track.find("title").text();
            this.appendChild(element);
        }.bind(tracklist));

        document.getElementById("player-tracks").addEventListener("click", function (event)
        {
            if (event.target.tagName === "LI")
            {
                tracklist.querySelector(".selected").setClass("selected", false);
                event.target.setClass("selected", true);
                playlist.seek(event.target.getAttribute("data-index"), player);
            }
        });

        function spcnext ()
        {
            if (playlist.hasNext(true))
            {
                spcplay(playlist.next(true));
            }
            else
            {
                player.dispatchEvent(new AudioStopEvent());
                tracklist.querySelector(".playing").setClass("playing", false);
                scroller.update(scroller.defaultText);
                playlist.synchronize();
                player = null;
            }
        }

        function spcplay (track)
        {
            var local = path + "/" + track.parentNode.parentNode.find("part > path").text();

            // See below why this is duplicated.
            player && player.pause();
            player && player.pause();
            player = audio(visualizer, scroller, local, track, spcnext, updating);
            player.once(Audio.HAVE_ENOUGH_DATA, player.play);

            var playing = tracklist.querySelector(".playing");
            if (playing)
            {
                playing.setClass("playing", false);
            }
            tracklist.querySelector(".selected").setClass("selected", false);
            tracklist.children[track.index].setClass("playing selected", true);
        };

        button(function ()
        {
            spcplay(playlist.previous());
        }, "player-previous");

        button(function ()
        {
            this.setClass("player-on", !playlist.straighten);

            if (playlist.straighten)
            {
                playlist = playlist.straighten();
            }
            else
            {
                playlist = playlist.shuffle();
            }
        }, "player-shuffle");

        button(function ()
        {
            if (player && !player.paused && playlist.index !== playlist.synchronize())
            {
                tracklist.querySelector(".playing").setClass("playing", false);
                player.pause();
                player.pause();
                player = null;
            }

            var track = playlist.current();
            var local = path + "/" + track.parentNode.parentNode.find("part > path").text();

            player = player || audio(visualizer, scroller, local, track, spcnext, updating);
            player.once(Audio.HAVE_ENOUGH_DATA, player.play);
            tracklist.children[playlist.current().index].setClass("playing", true);
        }, "player-play");

        button(function ()
        {
            // Dupe intentional! Firefox 29 seems to sometimes play the start of a song then stop.
            // If you pause it, it restarts! This is undesirable for many reasons.
            player && player.pause();
            player && player.pause();
        }, "player-pause");

        // TODO Support combined button. Update class name appropriately (player-pause, player-play).
        button(function ()
        {
        }, "player-playpause");

        button(function ()
        {
            visualizer && visualizer.unbind();
            if (player)
            {
                // See above. Dupe is intentional. If it restarts playback, we'll have already discarded
                // the reference and will be unable to stop it from playing at the same time as the next
                // audio element that is started up. An audio element will keep playing even if all refs
                // to it have been discarded (lost). So why multiple audio elements? Older Android audio
                // elements are apparently one-shots. Totally annoying.
                player.pause();
                player.pause();
                player.dispatchEvent(new AudioStopEvent());
                tracklist.querySelector(".playing").setClass("playing", false);
                scroller.update(scroller.defaultText);
            }
            playlist.synchronize();
            player = null;
        }, "player-stop");

        button(function ()
        {
            spcplay(playlist.next());
        }, "player-next");

        // Events for the progress control.
        progress = document.getElementById("player-progress");

        if (progress)
        {
            box = progress.getBoundingClientRect();
            time = document.getElementById("player-time");

            if (progress.tagName !== "PROGRESS" && progress.firstElementChild)
            {
                thumb = progress.firstElementChild;
            }

            window.addEventListener("resize", function ()
            {
                box = progress.getBoundingClientRect();
            }, false);

            progress.addEventListener("mousedown", function ()
            {
                updating.value = true;

                if (progress.setCapture)
                {
                    progress.setCapture();
                }
            });

            progress.addEventListener("mousemove", function (event)
            {
                var t;
                if (player && updating.value)
                {
                    t = Math.min(box.width, Math.max(0, event.pageX - Math.floor(box.left))) / progress.clientWidth
                            * player.duration;
                    (time || thumb || progress).textContent = timex(t);

                    if (thumb)
                    {
                        thumb.style.left = t / progress.max * progress.clientWidth + "px";
                    }
                }
            });

            progress.addEventListener("mouseup", function (event)
            {
                if (player && updating.value)
                {
                    player.currentTime = Math.min(box.width, Math.max(0, event.pageX - Math.floor(box.left)))
                            / progress.clientWidth * player.duration;
                }
                updating.value = false;
            });

            document.body.addEventListener("mouseup", function ()
            {
                updating.value = false;
            });
        }
    }

    function button (click, name)
    {
        var button = document.getElementById(name);
        if (button)
        {
            button.addEventListener("click", click, false);
        }
    }

    function audio (visualizer, scroller, path, track, next, updating)
    {
        var audio, progress, thumb, time, duration, title;

        title = track.find("name").text(", ") + " - " + track.find("title").text();

        if (scroller)
        {
            req(function (ramblings)
            {
                scroller.update(ramblings || title);
            }, path + "/" + track.find("filename").text() + ".txt", true);
        }

        audio = new Audio(path + "/" + track.find("filename").text() + ".mp3");
        audio.play = audio.play.bind(audio);
        audio.load();
        audio.addEventListener("ended", next, false);
        audio.addEventListener("error", function (error)
        {
            switch (error.target.error.code)
            {
                case MediaError.MEDIA_ERR_NETWORK:
                    warning("Network Error", "Unable to [completely] fetch the file for \"" + track.find("title").text()
                            + "\". If this problem persists, please report it to the maintainers.");
                    break;
                case MediaError.MEDIA_ERR_DECODE:
                    warning("Audio Decoding Error", "The file for \"" + track.find("title").text()
                            + "\" is either corruption problem or uses features this browser does not support.");
                    break;
                case MediaError.MEDIA_ERR_SRC_NOT_SUPPORTED:
                    warning("Audio Format Error", "The file for \"" + track.find("title").text()
                            + "\" is not a format this browser can play.");
           }
        }, false);
        visualizer && visualizer.bind(audio);

        // Progress bar and current location indicators.
        progress = document.getElementById("player-progress");
        time = document.getElementById("player-time");
        duration = document.getElementById("player-duration");

        (document.getElementById("player-now-playing") || {}).textContent = title;

        if (progress || duration)
        {
            if (progress && progress.tagName !== "PROGRESS" && progress.firstElementChild)
            {
                thumb = progress.firstElementChild;
            }

            audio.addEventListener("durationchange", function ()
            {
                if (progress)
                {
                    progress.max = audio.duration;
                }
                duration.textContent = timex(audio.duration);
            }, false);
        }

        if (progress || time)
        {
            audio.addEventListener("stop", function ()
            {
                if (progress)
                {
                    progress.value = 0;

                    if (thumb)
                    {
                        thumb.style.left = 0;
                    }
                }
                (time || thumb || progress).textContent = "00:00";

                updating.value = false;
            });

            audio.addEventListener("timeupdate", function ()
            {
                var t = audio.currentTime;

                if (!audio.paused && !updating.value)
                {
                    if (progress)
                    {
                        progress.value = Math.floor(t);

                        if (thumb)
                        {
                            thumb.style.left = progress.value / progress.max * progress.clientWidth + "px";
                        }
                    }

                    // Fallback to present same info in browsers not supporting
                    // the progress tag if the time element is not present.
                    (time || thumb || progress).textContent = timex(t);
                }
            }, false);
        }

        return audio;
    }

    //
    // Utilities

    function error (title, message)
    {
        document.body.insertAdjacentHTML("beforeend", "<div id='player-error'><h3>" + title + "</h3><p>"
                + message.replace(/\n\n/g, "</p><p>").replace(/\n/g, "<br>") + "</p></div>");

        document.body.lastChild.style.marginTop = document.body.lastChild.offsetHeight / -2 + "px";
        throw new Error("STOPPED");
    }

    // Quick difference: has close button, doesn't throw execution-stopping error.
    function warning (title, message)
    {
        document.body.insertAdjacentHTML("beforeend", "<div id='player-warning'><h3>" + title + "</h3>"
                + "<span class='close' title='close'>\u2715</span><p>"
                + message.replace(/\n\n/g, "</p><p>").replace(/\n/g, "<br>") + "</p></div>");

        document.body.querySelector("#player-warning > .close").addEventListener("click", function ()
        {
            this.parentNode.parentNode.removeChild(this.parentNode);
        }, false);

        document.body.lastChild.style.marginTop = document.body.lastChild.offsetHeight / -2 + "px";
    }

    function req (f, url, errorsNotFatal)
    {
        var xml, req = new XMLHttpRequest();

        xml = url.match(/\.xml$/);
        if (xml && req.overrideMimeType)
        {
            // file:///
            req.overrideMimeType("text/xml");
        }
        req.onreadystatechange = function ()
        {
            if (req.readyState === 4 && (req.status === 0 || req.status  === 200))
            {
                // 0 is for file:/// URLs. If responseXML is also null cross-domain rules blocked it and the catch will run next.
                if (req.status !== 0 || !xml || req.responseXML !== null)
                {
                    // Check for IE9. Default for responseXML in IE9 is an MSXML document, this returns a proper DOM Document.
                    if (xml && "selectNodes" in req.responseXML)
                    {
                        f(new DOMParser().parseFromString(req.responseText, "text/xml"));
                    }
                    else
                    {
                        f(xml ? req.responseXML : req.responseText);
                    }
                }
            }
            else if (req.readyState === 4)
            {
                if (errorsNotFatal)
                {
                    f(null, true);
                }
                else
                {
                    error("Request Error", "Unable to fetch " + url + "\n\nThis is unexpected. Please [reload the page and] try again. "
                            + "If this error persists, please report it to the maintainers.");
                }
            }
        };

        try
        {
            req.open("GET", url, true);
            req.send();
        }
        catch (_)
        {
            error("Request Error", "If this is running as from a file:/// path, your browser may be blocking requests to fetch other local files. "
                    + "This is not fixable. Please try the online version or run the native version instead.");
        }
    }

    function zeros (digits, number)
    {
        return (new Array(digits).join("0") + number).slice(-digits);
    }

    function timex (number)
    {
        if (number < 60)
        {
            return "00:" + zeros(2, Math.floor(number));
        }
        return zeros(2, Math.floor(number / 60)) + ":" + zeros(2, Math.floor(number % 60));
    }

    function text (node)
    {
        return node.firstChild.nodeValue;
    }

    function sanitize (text)
    {
        return text.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
    }

    function AudioStopEvent ()
    {
        var event = document.createEvent("CustomEvent");
        event.initCustomEvent("stop", false, false, undefined);
        return event;
    }

    // Playlists
    //

    function PlayList (songs, index)
    {
        if (!songs)
        {
            return;
        }
        this.songs = songs;
        this.index = index || 0;
        this.end = this.songs.length - 1;
    }

    PlayList.prototype.seek = function (index, soft)
    {
        index = parseInt(index);

        if (index !== this.index && index >= 0 && index <= this.end)
        {
            if (soft)
            {
                this.seekIndex = index;
                this.seeking = true;
            }
            else
            {
                this.index = index;
            }
        }
    };

    PlayList.prototype.synchronize = function ()
    {
        if (this.seeking)
        {
            this.index = this.seekIndex;
            this.seeking = false;
        }
        return this.index;
    };

    PlayList.prototype.current = function ()
    {
        return this.songs[this.index];
    };

    PlayList.prototype.previous = function ()
    {
        this.index = (!this.index ? this.songs.length : this.index) - 1;
        return this.songs[this.index];
    };

    PlayList.prototype.next = function (soft)
    {
        if (soft)
        {
            if (this.seeking)
            {
                this.index = this.seekIndex - 1;
            }
            else if (this.index === this.end)
            {
                return null;
            }
        }
        this.seeking = false;
        this.index = (this.index + 1) % (this.end + 1);
        return this.songs[this.index];
    };

    PlayList.prototype.hasNext = function (soft)
    {
        return (soft ? this.seeking || this.index < this.end : true);
    };

    PlayList.prototype.shuffle = function ()
    {
        return new ShuffledList(this.songs, this.index);
    };

    function ShuffledList (songs, index)
    {
        this._songs = songs;
        songs = songs.slice(0, index).concat(songs.slice(index + 1));

        // Want the mapping output otherwise this would work great as Array.prototype.shuffle
        this.map = (function ()
        {
            var i, j, t, map = {};

            for (i = this.length - 1; i > 0; i--)
            {
                j = Math.floor(Math.random() * (i + 1));
                t = this[i];
                this[i] = this[j];
                this[j] = t;
                map[this[i].index] = i + 1;
                map[this[j].index] = j + 1;
            }

            return map;
        }).call(songs);

        songs.unshift(this._songs[index]);
        this.map[this._songs[index].index] = 0;

        PlayList.call(this, songs, index);
    }
    ShuffledList.prototype = new PlayList();

    ShuffledList.prototype.seek = function (index, soft)
    {
        PlayList.prototype.seek.call(this, this.map[index], soft);
    };

    // Prevents shuffled play from ending after one song if you go backwards. Yes, this will have an
    // odd effect if you listen to the end of a shuffled playlist then go back one from the start.
    ShuffledList.prototype.previous = function ()
    {
        if ((this.end + 1) % this.songs.length === this.index)
        {
            this.end = (!this.end ? this.songs.length : this.end) - 1;
        }
        return PlayList.prototype.previous.call(this);
    };

    ShuffledList.prototype.next = function (soft)
    {
        // If it's a soft next, ignore the user's selected choice.
        if (soft)
        {
            this.seeking = false;
        }
        return PlayList.prototype.next.call(this, soft);
    };

    ShuffledList.prototype.hasNext = function (soft)
    {
        return (soft ? this.index < this.end : true);
    };

    ShuffledList.prototype.straighten = function ()
    {
        return new PlayList(this._songs, this.songs[this.index].index);
    };

    //
    // Built-in enhancements.

    try
    {
        Document.prototype.find = function (selector)
        {
            return this.querySelectorAll(selector);
        };
        Element.prototype.find = Document.prototype.find;

        // No classList property support until IE10. :(
        Element.prototype.setClass = function (name, add)
        {
            if (add && this.className.indexOf(name) === -1)
            {
                this.className += " " + name;
            }
            else if (!add)
            {
                this.className = this.className
                        .replace(new RegExp(name.split(" ").join("|"), "g"), "").replace(/\s+/g, " ");
            }
        };

        NodeList.prototype.map = function (f)
        {
            return Array.prototype.map.call(this, f);
        };

        NodeList.prototype.sort = function (f)
        {
            return Array.prototype.sort.call(Array.prototype.slice.call(this, 0), f);
        };

        NodeList.prototype.text = function (joiner)
        {
            return Array.prototype.map.call(this, text).join(joiner || "");
        };

        // Browsers don't define these on Audio but they do define similar
        // constants on MediaError ... quite strange. This normalizes that.
        Audio.HAVE_METADATA = HTMLAudioElement.HAVE_METADATA;
        Audio.HAVE_CURRENT_DATA = HTMLAudioElement.HAVE_CURRENT_DATA;
        Audio.HAVE_FUTURE_DATA = HTMLAudioElement.HAVE_FUTURE_DATA;
        Audio.HAVE_ENOUGH_DATA = HTMLAudioElement.HAVE_ENOUGH_DATA;

        Audio.Events = {};
        Audio.Events[Audio.HAVE_METADATA] = "loadedmetadata";
        Audio.Events[Audio.HAVE_CURRENT_DATA] = "loadeddata";
        Audio.Events[Audio.HAVE_FUTURE_DATA] = "canplay";
        Audio.Events[Audio.HAVE_ENOUGH_DATA] = "canplaythrough";

        Audio.prototype.once = function (what, f)
        {
            var w;

            if (this.readyState < what)
            {
                w = function ()
                {
                    this.removeEventListener(Audio.Events[what], w);
                    f();
                };
                this.addEventListener(Audio.Events[what], w, false);
            }
            else
            {
                f();
            }
        };
    }
    catch (_)
    {
        // The body tag may not be available at this point, so the error can't be shown until it is.
        (window.addEventListener || window.attachEvent)(window.addEventListener ? "load" : "onload", function ()
        {
            error("Error [of some kind]", "Er, hi? This application wasn't expecting to see you here. "
                    + "This error means it was unable to augment the browser's built-ins or your browser doesn't support items required by it. "
                    + "Try upgrading to the latest version of your browser or trying different one.");
        }, false);
    }

    //
    // Browser fun: http://www.paulirish.com/2011/requestanimationframe-for-smart-animating/

    (function ()
    {
        var lastTime = 0;
        var vendors = ["webkit", "moz"];

        for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x)
        {
            window.requestAnimationFrame = window[vendors[x] + "RequestAnimationFrame"];
            window.cancelAnimationFrame = window[vendors[x] + "CancelAnimationFrame"]
                    || window[vendors[x] + "CancelRequestAnimationFrame"];
        }

        if (!window.requestAnimationFrame)
        {
            window.requestAnimationFrame = function (callback, element)
            {
                var currTime = new Date().getTime();
                var timeToCall = Math.max(0, 16 - (currTime - lastTime));
                var id = window.setTimeout(function ()
                {
                    callback(currTime + timeToCall);
                }, timeToCall);
                lastTime = currTime + timeToCall;
                return id;
            };
        }

        if (!window.cancelAnimationFrame)
        {
            window.cancelAnimationFrame = function (id)
            {
                clearTimeout(id);
            };
        }
    }());


    //
    // Visualizer

    function Visualizer (container)
    {
        // Kick out early for prototype instantiations.
        if (!container)
        {
            return;
        }

        // Firefox only seems to allow values larger than 32; The spec requires values be a power
        // of two. Fail fast here on construction rather than later on connection after binding.
        if (!this.fftSize || this.fftSize < 32 || this.fftSize & (this.fftSize - 1))
        {
            throw new Error("Visualizer::fftSize must be >= 32 and a power of two");
        }

        this.container = container;
    }

    Visualizer.prototype._state = function ()
    {
        if (!this.audio || (this.audio.paused && !this.updateOnPause))
        {
            cancelAnimationFrame(this._request);
            this._request = null;
        }
        else if (!this._request && this.context)
        {
            this._request = requestAnimationFrame(this.update);
        }
    };

    // Don't call directly.
    Visualizer.prototype._connect = function ()
    {
        var i, channels, splitter, analyzer, source;

        if (!this.audio || !this.context)
        {
            throw new Error("Invalid state: Visualizer.bind not called first.");
        }

        channels = this.getDesiredChannelCount();
        splitter = this.context.createChannelSplitter(channels);

        for (i = 0; i < channels; i++)
        {
            analyzer = this.context.createAnalyser();
            analyzer.fftSize = this.fftSize;
            splitter.connect(analyzer, i, 0);

            this.channels[i] = analyzer;
        }

        source = this.context.createMediaElementSource(this.audio);
        source.connect(splitter);
        source.connect(this.context.destination);
    };

    Visualizer.prototype.bind = function (audio)
    {
        this.unbind();

        // Late binding so it doesn't get stuck with the this of the prototype instance.
        // If only addEventListener had a scope argument or this class was disposable ...
        // (It's not for subclasss with animations going even when paused or not playing.)
        if (!this._bound)
        {
            this._connect = this._connect.bind(this);
            this._state = this._state.bind(this);
            this.update = this.update.bind(this);
            this._bound = true;
        }

        this.context = window.AudioContext && new AudioContext();
        if (this.context)
        {
            this.audio = audio;
            this.audio.addEventListener("playing", this._state);
            this.audio.addEventListener("pause", this._state);
            this.audio.once(Audio.HAVE_FUTURE_DATA, this._connect);
        }
    };

    Visualizer.prototype.unbind = function ()
    {
        if (this.audio)
        {
            this.audio.removeEventListener("canplay", this._connect);
            this.audio.removeEventListener("playing", this._state);
            this.audio.removeEventListener("pause", this._state);
        }
        this.audio = null;
        this._state();
        this.context = null;
        this.channels = [];
    };

    // Overrideable stuff.
    Visualizer.prototype.getDesiredChannelCount = function ()
    {
        return 2;
    };
    Visualizer.prototype.fftSize = 32;
    Visualizer.prototype.updateOnPause = false;
    Visualizer.prototype.update = function (time)
    {
        this._request = requestAnimationFrame(this.update);
    };


    //
    // Simple two bars set Visualizer
    function BarViz (container)
    {
        Visualizer.call(this, container);

        this.container.innerHTML = "";
        this.container.className = "barviz";

        this.canvas = document.createElement("canvas");
        this.container.appendChild(this.canvas);

        this.ctx = this.canvas.getContext("2d");
        this.bars = this.fftSize / 2;

        window.addEventListener("resize", this.resize.bind(this), false);
        this.resize();

        req(function (text)
        {
            var found, matcher = /\s*#player-visualizer::bar[\s\r\n]*{[\r\n\s]*([^;\r\n]*)+/g;
            while (found = matcher.exec(text))
            {
                this.style = found.slice(1).map(function (property)
                {
                    return property.split(/\s*:[\r\n\s]*/);
                }).reduce(function (previous, item)
                {
                    previous[item[0]] = item[1];
                    return previous;
                }, this.style);

                this.ctx.fillStyle = this.style.fillStyle;
            }
        }.bind(this), styles);
    };
    BarViz.prototype = new Visualizer();

    BarViz.prototype.style = {
        strokeStyle: "white"
    };

    BarViz.prototype.resize = function ()
    {
        this.width = this.container.clientWidth;
        this.height = this.container.clientHeight;
        this.canvas.width = this.width;
        this.canvas.height = this.height;
        this.barWidth = (this.canvas.width - this.bars + 1) / this.bars;
        this.maxBarHeight = this.canvas.height / 2;

        this.ctx.fillStyle = this.style.fillStyle;
    };

    BarViz.prototype.unbind = function ()
    {
        Visualizer.prototype.unbind.call(this);
        this.ctx.clearRect(0, 0, this.width, this.height);
    };

    // Calculate a rudimentary FPS count.
    //this.fps = 1000 / (time - this.lastTime);
    //this.lastTime = time;
    BarViz.prototype.update = function (time)
    {
        Visualizer.prototype.update.call(this, time);

        this.ctx.clearRect(0, 0, this.width, this.height);

        var i, j, data, height;
        for (i = this.channels.length - 1; i >= 0; i--)
        {
            data = new Uint8Array(this.channels[i].frequencyBinCount);
            this.channels[i].getByteFrequencyData(data);

            for (j = data.length - 1; j >= 0; j--)
            {
                height = this.maxBarHeight * data[j] / 255;
                this.ctx.save();
                this.ctx.beginPath();
                this.ctx.rect(j * this.barWidth + j, i * this.maxBarHeight
                        + (this.maxBarHeight - height) * !(i % 2), this.barWidth, height);
                this.ctx.closePath();
                this.ctx.clip();
                this.ctx.fill();
                this.ctx.restore();
            }
        }
    };


    //
    // 3D visualizer made by joining left and right on two edges and populating the
    // grid based on the additions of both frequencies at the given grid cell.
    function BarGrid3DViz (container)
    {
        Visualizer.call(this, container);

        this.container.innerHTML = "";
        this.container.className = "bargrid3dviz";

        if (window.WebGLRenderingContext)
        {
            this.renderer = new THREE.WebGLRenderer({
                alpha: true
            });
        }
        else
        {
            this.renderer = new THREE.CanvasRenderer({
                alpha: true
            });
        }
        this.container.appendChild(this.renderer.domElement);

        window.addEventListener("resize", this.resize.bind(this));
        this.resize();

        req(function (text)
        {
            var blocks, props, parts, block, prop;

            blocks = /\s*#player-visualizer::bar3d[\s\r\n]*{[\r\n\s]*([^}]+)[\s\r\n]*/g;
            props = /\s*([^;]+)/g;
            parts = /\s*:[\r\n\s]*/;

            while (block = blocks.exec(text))
            {
                while (prop = props.exec(block[1]))
                {
                    this.style = prop.slice(1).map(function (property)
                    {
                        return property.split(parts);
                    }).reduce(function (previous, item)
                    {
                        var value = item[1];
                        switch (value)
                        {
                            case "true":
                                value = true;
                                break;
                            case "false":
                                value = false;
                                break;
                            default:
                                value = (value && value.indexOf("0x") === 0 ? parseInt(value) : parseFloat(value));
                        }
                        previous[item[0]] = value;
                        return previous;
                    }, this.style);
                }
            }

            this.resize();
        }.bind(this), styles);
    };
    BarGrid3DViz.prototype = new Visualizer();

    BarGrid3DViz.prototype.style = {
        ambient: 0x030303,
        color: 0xdddddd,
        specular: 0x009900,
        shininess: 30,
        transparent: true,
        opacity: .5
    };

    BarGrid3DViz.prototype.maxBarHeight = 100;

    BarGrid3DViz.prototype.updateOnPause = true;

    BarGrid3DViz.prototype.resize = function ()
    {
        this.width = this.container.clientWidth;
        this.height = this.container.clientHeight;
        this.renderer.setSize(this.width, this.height);

        var z, x, v, min, material, geometry, mesh, size;

        min = Math.min(this.width, this.height) * 4; // FIXME! x4 is bad.
        size = min / this.fftSize;

        material = new THREE.MeshPhongMaterial({
            ambient: this.style.ambient,
            color: this.style.color,
            specular: this.style.specular,
            shininess: this.style.shininess,
            transparent: this.style.transparent,
            opacity: this.style.opacity
        });

        this.camera = new THREE.PerspectiveCamera(75, this.width / this.height, 1, 10000);
        this.camera.position.z = 1000;

        this.scene = new THREE.Scene();

        this.group = new THREE.Object3D();
        this.group.rotation.x = -75;

        var mesh = new THREE.Mesh(new THREE.BoxGeometry(min, 0, min), material);
        mesh.matrixAutoUpdate = false;
        mesh.updateMatrix();
        this.group.add(mesh);

        this.objects = [];

        for (z = 0; z < this.fftSize; z += 2)
        {
            if (!this.objects[z / 2])
            {
                this.objects.push([]);
            }
            for (x = 0; x < this.fftSize; x += 2)
            {
                geometry = new THREE.BoxGeometry(size, 1, size);
                geometry.dynamic = true;
                geometry.top = [];

                for (v = geometry.vertices.length - 1; v >= 0; v--)
                {
                    if (geometry.vertices[v].y > 0)
                    {
                        geometry.top.push(geometry.vertices[v]);
                    }
                    else
                    {
                        geometry.vertices[v].y = 0;
                    }
                }

                mesh = new THREE.Mesh(geometry, material);
                mesh.position.x = x * size - min / 2 + size;
                mesh.position.z = z * size - min / 2 + size;
                mesh.matrixAutoUpdate = false;
                mesh.updateMatrix();
                this.group.add(mesh);

                this.objects[z / 2][x / 2] = mesh;
            }
        }
        this.scene.add(this.group);

        var light = new THREE.DirectionalLight(0xffffff, .5);
        light.position.set(0, 1, 1);
        this.scene.add(light);
    };

    BarGrid3DViz.prototype.getDataAt = function (z, x)
    {
        return this.dataZ[z] + this.dataX[x];
    };

    BarGrid3DViz.prototype.update = function (time)
    {
        Visualizer.prototype.update.call(this, time);

        this.group.rotation.y += .01;

        this.dataZ = new Uint8Array(this.channels[0].frequencyBinCount);
        this.channels[0].getByteFrequencyData(this.dataZ);

        this.dataX = new Uint8Array(this.channels[1].frequencyBinCount);
        this.channels[1].getByteFrequencyData(this.dataX);

        var z, x = 0, v;
        for (z = this.objects.length - 1; z >= 0; z--)
        {
            for (x = this.objects[z].length - 1; x >= 0; x--)
            {
                for (v = this.objects[z][x].geometry.top.length - 1; v >= 0; v--)
                {
                    this.objects[z][x].geometry.top[v].y = this.maxBarHeight * this.getDataAt(z, x) / 256;
                }
                this.objects[z][x].geometry.verticesNeedUpdate = true;
            }
        }

        this.renderer.render(this.scene, this.camera);
    };


    //
    // Scroller

    function Scroller (container)
    {
        // Kick out early for prototype instantiations.
        if (!container)
        {
            return;
        }

        this.container = container;
    }

    Scroller.prototype.update = function (text)
    {
        this.container.textContent = text;
    };


    //
    // Scroller implementation that actually does something.
    function BasicAnimatedScroller (container)
    {
        Scroller.call(this, container);

        this.content = document.createElement("span");
        this.container.appendChild(this.content);

        window.addEventListener("resize", this.resize.bind(this));
    }
    BasicAnimatedScroller.prototype = new Scroller();

    BasicAnimatedScroller.prototype.animationName = "player-scroller-animation";
    BasicAnimatedScroller.prototype.animationDelay = 10;

    BasicAnimatedScroller.prototype.resize = function ()
    {
        var i, styles, length, declaration, animation, prefixes = ["-webkit-", "-moz-", ""];

        if (this.styles)
        {
            this.styles.parentNode.removeChild(this.styles);
        }

        styles = "";
        declaration = "";
        length = Math.max(15, 15 * this.content.clientWidth / 1160);

        animation = "keyframes " + this.animationName
                + "{0%{left:100%}" + (length / (length + this.animationDelay) * 100).toFixed(2) + "%{left:"
                + (-this.content.offsetWidth - 10) + "px}100%{left:" + (-this.content.offsetWidth - 10) + "px}}";

        for (i = 0; i < prefixes.length; i++)
        {
            declaration += prefixes[i] + "animation:" + this.animationName + " "
                + (length + this.animationDelay).toFixed(2) + "s linear infinite;";

            styles += "@" + prefixes[i] + animation;
        }
        styles = "#player-scroller>span{" + declaration + "}" + styles;

        document.head.insertAdjacentHTML("beforeend", "<style>" + styles + "</style>");
        this.styles = document.head.lastElementChild;
    };

    BasicAnimatedScroller.prototype.update = function (text)
    {
        this.content.textContent = text;
        this.resize();
    };

    BasicAnimatedScroller.isSupported = function ()
    {
        return "animation" in document.body.style || "webkitAnimation" in document.body.style
                || "MozAnimation" in document.body.style;
    };
}());
